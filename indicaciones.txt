acaps
	id
	country
	iso
	region
	log_type
	category
	measure
	targeted_pop_group
	comments
	non_compliance
	date_implemented
	
coronavirus world airport impacts
	objectid
	type
	name
	latitude_d
	longitude
	geometry
	iso_countr
	scheduled
	iata_code
	status
	
covid19 by country
	country
	countryAlpha3code
	Date
	GTRStringencyIndex
	confirmed_PopPct
	deaths_PopPct
	recoveries_PopPct
	



covid-19
	TODOS menos los que van desde confirmed_Cases hasta el final pero excluyendo a 
		pop_age_above_65_percentage
	

global school closures covid 19  -- WORK WITH IT AS AN INDEPENDENT FILE



-------------

covid19 country population - is not gonna be used
current-data-on-the-geographic-distribution-of-covid-19-cases-worldwide - is not gonna be used
global_mobility report -- is not gonna be used







OBSERVACIONES E IDEAS
- Hacer clusterización entre los países y ver donde cae Ecuador y tomar sus estrategias. 
- El archivo SCHOOLS es opcional, en caso de que falte ideas o se quiera analizar el cierre de escuelas en un tiempo desde mediados de febrero a mediados de abril.



UN TRABAJO
- Clusterizar a los países en los nuevos datasets. YA
- Tomar a 3-5 países de cada cluster (cambiar en caso de que lo amerite).  YA
- Analizar a todos esos países seleccionados con la cantidad de los casos de COVID-19 (gráficas) (conclusiones)
- Según eso analizar la estrategia que implementaron estos países (paises con pocos casos): GOV MEASURES - trabajar como un archivo independiente en donde se analicen las medidas tomadas con alguna técnica de identificación de texto, o data mining. 
- Sacar grafica de las restricciones tomadas en los aeropuertos. YA    
- grafica de principales medidas tomadas en general. YA





----------------------------------------- PROCESO LLEVADO A CABO ---------------------------------------------------------

ARCHIVOS YA UTILIZADOS:
    ** schools.csv (aún no se usa, es opcional)
    - government-measures.csv
    - variables.csv
    - airport-impacts.csv
    - covid_by_country.csv
    - mobility.csv


RECOLECCIÓN DE DATOS
- Se obtuvieron los archivos desde Kaggle (poner los enlaces)

DATA CLEANING

1. Se hizo Eliminación de variables aunque estse paso en DATA TRANSFORMING pero decidí hacerlo primero. De los archivos que se van a usar para el proyecto, se elimina las variables(columnas) que no serán usadas.
2. Se controlaron los missing values, outliers, typos, etc.
3. Se extrajo nuevas variables que derivaron de la fecha y del número de casos en el archivo (covid_by_country)


ESTADÍSTICAS DE MEDIDAS TOMADAS POR EL GOBIERNO (sale del archivo government measures. Esto se hace para poner en contexto, ya que las gráficas son un tanto obvias.)

1. Se analizaron las categorías en las que se implementó medidas (poner ESTA imagen primero cuando se exponga)
2. Se analizaron las 10 principales medidas tomadas en general por los países en todo el mundo.
3. Se analizaron las tendencias en los aeropuertos en todo el mundo.

CLUSTERIZACIÓN

1. Se agruparon los países según sus similitudes. Esto se hizo mediante la toma de determinadas variables. Aquí no influyeron los casos de COVID, ya que las variables corresponden a estadísticas de cada país lo que brinda una visión más clara de pais. Esta técnica comprendió una clusterización no supervisada mediante Kmeans. No se especificó un determinado grupo, más bien el propio modelo generó la cantidad.


ANÁLISIS DE CASOS COVID

1. Analizar la tendencia de casos de los países similares a Ecuador. (covid19_by_country.csv) por semana
2. En los países que fueron reduciendo sus casos por semana, separarlos en otro grupo para luego tomar sus medidas implementadas.
    - En este paso el que menor PORCENTAJE de contagios demostró fue Japón. Los otrs 5 países tuvieron curvas diferentes a la de Ecuador lo cual no sirve para este análisis. Japón, tuvo una curva menor incluso menor que Ecuador. Por ende, se consideró a Japón para realizar análisis posteriores. Analizar algún rango de semanas específicas.
3. Ver la mobilidad (mobility.csv) en esos países en las semanas que sus casos bajaron. 

TOMA DE DECISIONES (falta)

1. Seleccionar las estrategias que mejor se adapten y que se tenga evidencia de que han sido efectivas.















    

