**ANÁLISIS COVID-19 EN BASE A DATOS DE MOVILIDAD, CARACTERÍSTICAS DE PAISES Y CASOS POSITIVOS.**

**Autor: Edison Sanango**

En este proyecto se buscó determinar las principales medidas y/o restricciones que se pueden tomar en Ecuador en comparación  a otros países a nivel mundial, en base a datos recolectados se concluyeron los siguientes puntos:

- Aproximadamente el 40% de vuelos a nivel mundial fueron suspendidos o registraron restricciones.

<img src="DataScienceProcess/Images/AirportsRestricions.png" style="height: 50px; width:50px;"/>

- Por otra parte, las medidas que más se tomaron a nivel mundial son medidas económicas y de salud.

<img src="DataScienceProcess/Images/MedidasTomadasANivelMundial.png" style="height: 50px; width:50px;"/>

- Las categorías en las que más impactaron las medidas tomadas anteriormente fueron las siguientes:

<img src="DataScienceProcess/Images/CategoriasEnLasqueseTomoMedidas.png" style="height: 50px; width:50px;"/>

- Al ver que los datos reflejan un cambio en el diario vivir a nivel mundial como lo proyectaron las anteriores gráficas, se determinó cuáles países comparten una realidad 'similar'. Se determinaron grupos de países que son similares entre ellos.

<img src="DataScienceProcess/Images/GraficaPaisesSimilares.png" style="height: 50px; width:50px;"/>

- Al aislar al grupo de Ecuador se observó que los países en el mismo estado que Ecuador son: Perú, Bolivia, España, Italia, Japón, Suecia, Países Bajos, República Checa, entre otros.

<img src="DataScienceProcess/Images/SimilaresEcuador.png" style="height: 50px; width:50px;"/>

- De los países que están en el mismo grupo que Ecuador se obtuvo la cantidad de casos positivos de COVID de los principales países durante el 2020 agrupados por semana.

<img src="DataScienceProcess/Images/CasosCovidAnalizados.png" style="height: 50px; width:50px;"/>

- Después de ver la tendencia de los casos en cada país se determinó que el país 'modelo' a seguir por Ecuador es Japón, por lo cual se obtuvo un ranking de las medidas tomadas en Japón como posibles opciones a considerar en Ecuador, las cuales fueron:

<img src="DataScienceProcess/Images/MedidasEnJapon.png" style="height: 50px; width:50px;"/>

- Con los datos de la anterior gráfica se determinó que un aspecto importante fue la cuarentena y medidas de distanciamento, por lo cual, se analizaron los datos de movilidad de Japón agrupados por semana del año 2020.

<img src="DataScienceProcess/Images/CambiosMovilidadJapn.png" style="height: 50px; width:50px;"/>

**Conclusiones:** 

- Se determinó que el cuidado a las estaciones de tránsito y los puntos de trabajo presenciales son las mejores estrategias que puede implementar Ecuador con respecto al COVID19. 

- Las medidas sanitarias como cuarentenas y confinamientos deben mantenerse e incluso reforzarse. 

- Los sectores de recreación y ventas no deben sufrir un cambio significativo en sus operaciones normales ya que no representa un grave riesgo.









